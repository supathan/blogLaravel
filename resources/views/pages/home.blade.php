@extends('index')

@section('title','| Home')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="jumbotron">
            <h1 class="display-4">Welcome to my website</h1>
            <p class="lead">Thank you for visiting to my website built with bootstrap 5.6 . Keep yourself something here to see my latest post</p>
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
        </div>
    </div>
</div> <!-- end of header row -->


<div class="row">
    <div class="col-md-8">

        <div class="post">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat....</p>
            <a href="#" class="btn btn-primary">Read More</a>
        </div>
        <hr>
        <div class="post">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat....</p>
            <a href="#" class="btn btn-primary">Read More</a>
        </div>
        <hr>
        <div class="post">
            <h1>Title</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat....</p>
            <a href="#" class="btn btn-primary">Read More</a>
        </div>
    </div>
    <div class="col-md-3 col-offset-1">
        <h1> Sidebar</h1>
    </div>

</div>

@endsection
