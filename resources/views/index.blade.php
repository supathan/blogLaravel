<!doctype html>
<html lang="en">
<head>
    @include('partials._header')
</head>
<body>

    @include('partials._nav')

    <div class="container">

        @yield('content')

    </div> <!--End of container -->

    @include('partials._script')

    @yield('scripts')

</body>
</html>
